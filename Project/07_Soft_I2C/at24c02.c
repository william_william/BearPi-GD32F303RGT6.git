#include "at24c02.h"

#define AT24C02_Addr 0x50       /* AT24C02的7位器件地址 */

#define WRITE_BIT 0x00          /*!< I2C master write */
#define READ_BIT 0x01           /*!< I2C master read */


/* 描述：AT24C02初始化
 * 参数：无
 * 返回值：无*/	
void at24c02_init(void)
{
	Soft_I2C_Init();
}

/* 描述：在AT24C02指定地址读出一个字节的数据
 * 参数：ReadAddr: 需要读出数据的地址
 *		 ReadByte: 读出的数据值的存放指针
 * 返回值：0：读取成功 		其他：读取错误*/
uint8_t AT24C02_Read_Byte(uint16_t ReadAddr, uint8_t *ReadByte)
{	
	uint8_t err = 0;
    IIC_Start();  
	IIC_SendByte((AT24C02_Addr<<1) | WRITE_BIT);   	/* 发送器件地址,写数据 */ 	 
	err |= IIC_wait_ACK(); 
    IIC_SendByte(ReadAddr);   						/* 发送要读出数据的地址 */
	err |= (IIC_wait_ACK() << 1);
	IIC_Start();  	 	   
	IIC_SendByte((AT24C02_Addr<<1) | READ_BIT);     /* 进入接收模式	*/		   
	err |= (IIC_wait_ACK() << 2);
    *ReadByte = IIC_RcvByte();
	IIC_NACK();										/* 发送非应答 NACK */
    IIC_Stop();										/*产生一个停止条件*/	    
	return err;
}

/* 描述：在AT24C02指定地址写入一个字节的数据
 * 参数：WriteAddr: 需要写入数据的地址
 *		 WriteByte: 要写入的数据
 * 返回值：0：写入成功 		其他：写入错误*/
uint8_t AT24C02_Write_Byte(uint16_t WriteAddr,uint16_t WriteByte)
{	
	uint8_t err = 0;	
    IIC_Start();  
	IIC_SendByte((AT24C02_Addr<<1) | WRITE_BIT);   	/* 发送器件地址,写数据 */ 	 	 
	err |= IIC_wait_ACK(); 
    IIC_SendByte(WriteAddr);   						/* 发送要写入数据的地址 */
	err |= (IIC_wait_ACK() << 1);									  		   
	IIC_SendByte(WriteByte);     					/* 写入数据 */					   
	err |= (IIC_wait_ACK() << 2);
    IIC_Stop();//产生一个停止条件 
	return err;
}
