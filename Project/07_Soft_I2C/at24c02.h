#ifndef __AT24C02_H
#define __AT24C02_H

#include "main.h"
#include "soft_i2c.h"

void at24c02_init(void);
uint8_t AT24C02_Read_Byte(uint16_t ReadAddr, uint8_t *ReadByte);
uint8_t AT24C02_Write_Byte(uint16_t WriteAddr,uint16_t WriteByte);


#endif

