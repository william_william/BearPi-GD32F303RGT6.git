#include "main.h"
#include "led.h"
#include "uart.h"
#include "timer.h"

int main(void)
{
	/* 配置系统时钟 */
	systick_config();
	/* 初始化LED */
	// led_init();
	/* 初始化USART0 */
	uart0_init(115200);
	/* 初始化定时器2 */
	timer2_pwm_init(120-1, 1000-1, 300);
	/* 初始化定时器5,3ms */
	timer5_init(1200-1, 300-1);
	
	/* 通过串口打印 Hello world! */
	u1_printf("Hello world! ");
	u1_printf("I am William. \r\n");
	
    while(1)
	{
		if(UART0_RX_STAT > 0)
		{
			UART0_RX_STAT = 0;
			u1_printf("RECEIVE %d data:%s \r\n", UART0_RX_NUM, UART0_RX_BUF);
		}
		
		delay_1ms(10);
		
//		if(dir) pwmval++;				// dir==1  pwmval递增
//		else pwmval--;				    // dir==0  pwmval递减
//		if( pwmval>500 ) dir=0;			// pwmval到达500后，方向为递减
//		if( pwmval==0 ) dir=1;			// pwmval递减到0后，方向改为递增
//		TIMER_CH2CV(TIMER2) =  pwmval; 	// 修改比较值，修改占空比
//        if( pwmval==0 ) delay_1ms(300);
//		delay_1ms(3);
	}
}

