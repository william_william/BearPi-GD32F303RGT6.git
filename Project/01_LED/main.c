#include "main.h"
#include "led.h"

int main(void)
{
	/* 配置系统时钟 */
	systick_config();
	/* 初始化LED */
	led_init();

    while(1)
	{		
        /* turn on LED */
        LED(1);
        delay_1ms(1000);

        /* turn off LED */
        LED(0);
        delay_1ms(1000);
    }
}

