#ifndef __USART_H
#define __USART_H

#include <stdio.h>
#include <string.h>	
#include <stdarg.h>
#include "main.h"

/* 串口0初始化，参数为波特率 */
void uart0_init(uint32_t bound);
/* 自定义UART0 printf 函数 */
void u1_printf(char* fmt,...);

#ifdef UART0_RX
/* 接收缓存 */
extern uint8_t UART0_RX_BUF[]; 		/* 双接收缓冲区 */
extern uint8_t UART0_RX_STAT;		/* 接受状态 0x01:已接收到数据  0x03:接收缓冲区半满  0x07:接收缓冲区全满 */
extern uint32_t UART0_RX_NUM;		/* 接收到的数据个数 */
#endif

#endif
