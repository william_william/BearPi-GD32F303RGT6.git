#ifndef __TIMER_H
#define __TIMER_H

#include "main.h"

void timer5_init(uint32_t psr, uint32_t arr);
void timer2_pwm_init(uint32_t psr, uint32_t arr, uint32_t duty);

#endif

