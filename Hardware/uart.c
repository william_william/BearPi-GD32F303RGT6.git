/* 串口硬件初始化 */
#include "uart.h"

/* 发送缓存 */
#define UART0_TX_LEN		256				/* 单次最大发送缓存字节数 */
uint8_t UART0_TX_BUF[UART0_TX_LEN]; 		/* 发送缓冲区 */
uint8_t DMA_BUF_BUSY = 0 ;					/* 缓冲区是否已被占用 */

#ifdef UART0_RX
/* 接收缓存 */
#define UART0_RX_LEN		256				/* 单个缓存区字节数 */
uint8_t UART0_RX_BUF[UART0_RX_LEN*2]; 		/* 双接收缓冲区 */
uint8_t UART0_RX_STAT = 0;					/* 接受状态 0x01:已接收到数据  0x03:接收缓冲区半满  0x07:接收缓冲区全满 */
uint32_t UART0_RX_NUM = 0;					/* 接收到的数据个数 */
#endif

/* USART0串口初始化函数
 * 参数：波特率
 * 返回值：无	*/
void uart0_init(uint32_t bound)
{	
	/* 使能 GPIOA 时钟 */
    rcu_periph_clock_enable(RCU_GPIOA);

    /* 使能 USART0 时钟 */
    rcu_periph_clock_enable(RCU_USART0);

    /* PA9  复用为 USART0_Tx */
    gpio_init(GPIOA, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ,GPIO_PIN_9);

    /* PA10 复用为 USARTx_Rx */
    gpio_init(GPIOA, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_50MHZ,GPIO_PIN_10);
					
    /* USART0 初始化配置 */
    usart_deinit(USART0);
    usart_baudrate_set(USART0, bound);						/* 设置波特率 */
    usart_receive_config(USART0, USART_RECEIVE_ENABLE);		/* 使能接收 */
    usart_transmit_config(USART0, USART_TRANSMIT_ENABLE);	/* 使能发送 */	
	usart_enable(USART0);									/* 使能串口0 */
	

#ifdef UART0_DMA
	/* 定义一个DMA配置结构体 */
	dma_parameter_struct dma_init_struct;
    /* 使能 DMA 时钟 */
    rcu_periph_clock_enable(RCU_DMA0);
	
	/* 初始化 DMA0 通道3 */
    dma_deinit(DMA0, DMA_CH3);
    dma_init_struct.direction = DMA_MEMORY_TO_PERIPHERAL;		/* 存储器到外设方向 */
    dma_init_struct.memory_addr = (uint32_t)UART0_TX_BUF;		/* 存储器基地址 */
    dma_init_struct.memory_inc = DMA_MEMORY_INCREASE_ENABLE;	/* 存储器地址自增 */
    dma_init_struct.memory_width = DMA_MEMORY_WIDTH_8BIT;		/* 存储器位宽为8位 */
    dma_init_struct.number = UART0_TX_LEN;						/* 传输数据个数 */
    dma_init_struct.periph_addr = ((uint32_t)0x40013804);		/* 外设基地址，即USART数据寄存器地址 */
    dma_init_struct.periph_inc = DMA_PERIPH_INCREASE_DISABLE;	/* 外设地址固定不变 */
    dma_init_struct.periph_width = DMA_PERIPHERAL_WIDTH_8BIT;	/* 外设数据位宽为8位 */
    dma_init_struct.priority = DMA_PRIORITY_ULTRA_HIGH;			/* 软件优先级为极高*/
    dma_init(DMA0, DMA_CH3, &dma_init_struct);
    
    /* DMA循环模式配置，不使用循环模式 */
    dma_circulation_disable(DMA0, DMA_CH3);
	/* DMA存储器到存储器模式模式配置，不使用存储器到存储器模式*/
    dma_memory_to_memory_disable(DMA0, DMA_CH3);

    /* USART DMA 发送使能 */
    usart_dma_transmit_config(USART0, USART_DENT_ENABLE);
	/* DMA0 通道3 中断优先级设置并使能 */
	nvic_irq_enable(DMA0_Channel3_IRQn, 0, 0);
	/* 使能 DMA0 通道3 传输完成、传输错误中断 */
    dma_interrupt_enable(DMA0, DMA_CH3, DMA_INT_FTF|DMA_INT_ERR);
    /* 使能 DMA0 通道3 */
    dma_channel_enable(DMA0, DMA_CH3);
#endif

#ifdef UART0_RX
#ifndef UART0_DMA
	/* 定义一个DMA配置结构体 */
	dma_parameter_struct dma_init_struct;
    /* 使能 DMA 时钟 */
    rcu_periph_clock_enable(RCU_DMA0);
#endif
	
	/* 初始化 DMA0 通道4 */
    dma_deinit(DMA0, DMA_CH4);
    dma_init_struct.direction = DMA_PERIPHERAL_TO_MEMORY;		/* 外设到存储器方向 */
    dma_init_struct.memory_addr = (uint32_t)UART0_RX_BUF;		/* 存储器基地址 */
    dma_init_struct.memory_inc = DMA_MEMORY_INCREASE_ENABLE;	/* 存储器地址自增 */
    dma_init_struct.memory_width = DMA_MEMORY_WIDTH_8BIT;		/* 存储器位宽为8位 */
    dma_init_struct.number = UART0_RX_LEN*2;					/* 传输数据个数 */
    dma_init_struct.periph_addr = ((uint32_t)0x40013804);		/* 外设基地址，即USART数据寄存器地址 */
    dma_init_struct.periph_inc = DMA_PERIPH_INCREASE_DISABLE;	/* 外设地址固定不变 */
    dma_init_struct.periph_width = DMA_PERIPHERAL_WIDTH_8BIT;	/* 外设数据位宽为8位 */
    dma_init_struct.priority = DMA_PRIORITY_ULTRA_HIGH;			/* 软件优先级为极高*/
    dma_init(DMA0, DMA_CH4, &dma_init_struct);
    
    /* DMA循环模式配置，不使用循环模式 */
    dma_circulation_disable(DMA0, DMA_CH4);
	/* DMA存储器到存储器模式模式配置，不使用存储器到存储器模式*/
    dma_memory_to_memory_disable(DMA0, DMA_CH4);

    /* USART DMA 发送和接收使能 */
    usart_dma_transmit_config(USART0, USART_DENT_ENABLE|USART_DENR_ENABLE);
	/* DMA0 通道4 中断优先级设置并使能 */
	nvic_irq_enable(DMA0_Channel4_IRQn, 0, 0);
	/* 使能 DMA0 通道4 半传输、传输完成、传输错误中断 */
    dma_interrupt_enable(DMA0, DMA_CH4, DMA_INT_FTF|DMA_INT_HTF|DMA_INT_ERR);
    /* 使能 DMA0 通道4 */
    dma_channel_enable(DMA0, DMA_CH4);
	
	/* USART中断设置，抢占优先级0，子优先级0 */
	nvic_irq_enable(USART0_IRQn, 0, 0); 
	/* 使能USART0空闲中断 */
    usart_interrupt_enable(USART0, USART_INT_IDLE);
#endif

}

#ifdef UART0_DMA
/* 自定义UART0 printf 函数
 * 参数：带发送的字符串，确保一次发送数据不超过UART0_TX_LEN字节
 * 返回值：无	*/
void u1_printf(char* fmt,...)  
{
	uint32_t i;
	va_list ap; 
	va_start(ap,fmt);
	
	while(DMA_BUF_BUSY == 1);
	DMA_BUF_BUSY = 1;
	
	vsprintf((char*)UART0_TX_BUF,fmt,ap);
	va_end(ap);
	UART0_TX_BUF[UART0_TX_LEN-1] = '\0';
	
	/* 计算此次发送数据的长度 */
	i=strlen((const char*)(UART0_TX_BUF));
	
	/* 设置DMA传输 */
	dma_channel_disable(DMA0, DMA_CH3);		/* 关闭DMA传输才可以进行设置 */
	dma_memory_address_config(DMA0,DMA_CH3,(uint32_t)(UART0_TX_BUF));
	dma_transfer_number_config(DMA0,DMA_CH3,i);
	dma_channel_enable(DMA0, DMA_CH3);		/* 开启DMA传输 */
}
#else
/* 加入以下代码,支持printf函数,而不需要选择use MicroLIB	*/  
#pragma import(__use_no_semihosting)             
/* 标准库需要的支持函数 */                
struct __FILE 
{ 
	int handle; 

}; 

FILE __stdout;       
/* 定义_sys_exit()以避免使用半主机模式  */  
void _sys_exit(int x) 
{ 
	x = x; 
} 

/* 重定义fputc函数 */
int fputc(int ch, FILE *f)
{   
	while (RESET == usart_flag_get(USART0, USART_FLAG_TBE))
		;	
	usart_data_transmit(USART0, (uint8_t)ch);

	return ch;
}
#endif


/* DMA0 通道3 中断服务函数
 * 参数：无
 * 返回值：无	*/
void DMA0_Channel3_IRQHandler(void)
{
	/* 清除DMA0 通道3 中断标志位 */
	dma_interrupt_flag_clear(DMA0, DMA_CH3, DMA_INT_FLAG_G);
	/* 进入中断，表示已经传输完成缓冲区，释放缓冲区 */
	if(DMA_BUF_BUSY == 1) DMA_BUF_BUSY = 0;
}

#ifdef UART0_RX
/* USART0 中断服务函数
 * 参数：无
 * 返回值：无	*/
/* 串口0中断服务程序 */
void USART0_IRQHandler(void)	
{
    if(RESET != usart_interrupt_flag_get(USART0, USART_INT_FLAG_IDLE)) //空闲中断
	{
		usart_interrupt_flag_clear(USART0,USART_INT_FLAG_IDLE);	/* 清除空闲中断标志位 */
		usart_data_receive(USART0);								/* 清除接收完成标志位 */
		dma_channel_disable(DMA0, DMA_CH4);						/* 关闭DMA传输 */
		
		UART0_RX_NUM = sizeof(UART0_RX_BUF) - dma_transfer_number_get(DMA0,DMA_CH4);
		UART0_RX_BUF[UART0_RX_NUM] = '\0';	/* 添加字符串结束符 */
		UART0_RX_STAT = 0x01;				/* 接受状态 0x01:已接收到数据 */
		
		/* 重新设置DMA传输 */
		dma_memory_address_config(DMA0,DMA_CH4,(uint32_t)UART0_RX_BUF);
		dma_transfer_number_config(DMA0,DMA_CH4,sizeof(UART0_RX_BUF));
		dma_channel_enable(DMA0, DMA_CH4);		/* 开启DMA传输 */
    }
}


/* DMA0 通道4 中断服务函数
 * 参数：无
 * 返回值：无	*/
void DMA0_Channel4_IRQHandler(void)
{
	/* 清除DMA0 通道4 中断标志位 */
	dma_interrupt_flag_clear(DMA0, DMA_CH4, DMA_INT_FLAG_G);
	if(dma_interrupt_flag_get(DMA0, DMA_CH4, DMA_INT_FLAG_HTF))
	{
		UART0_RX_NUM = UART0_RX_LEN;
		UART0_RX_BUF[UART0_RX_LEN] = '\0';		/* 添加字符串结束符 */
		UART0_RX_STAT = 0x03;					/* 接受状态 0x03:接收缓冲区半满 */
	}
	if(dma_interrupt_flag_get(DMA0, DMA_CH4, DMA_INT_FLAG_FTF))
	{
		UART0_RX_NUM = UART0_RX_LEN*2;
		UART0_RX_BUF[UART0_RX_LEN*2-1] = '\0';	/* 添加字符串结束符 */
		UART0_RX_STAT = 0x07;					/* 接受状态 0x07:接收缓冲区全满 */
	}
}

#endif
